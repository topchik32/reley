#include <iostream>
#include <fstream>
#include <vector>
#include <time.h>
using namespace std;
ofstream file("gai.txt");
long double x0 = 21.2396e-6, sigma = 0.0727361, dens = 998.195;//20*1e-6
// mashtabu
long double tm = sqrt(dens*x0*x0*x0 / sigma);
long double pm = sigma / x0; // 
long double mum = pm * tm;
long double vm = x0 / tm;
//bezrazmernaue velichinu
long double mu = 0.00100044/mum, vzv = 1486.97 /vm;         //mu = 0.001    sigma = 71.5 * 0.001 Ga
long double pb, pb_, pA = 1.2e5/pm, pg ,usk = 0, usk_ = 0;
long double pa = pA;
long double x = 1, v = 0, x_ = x, v_ = 0;
long double k = 1.333;//polytropic
long double pbesk = 1e5/(pm);
//1.2 * 100000, 10000 *1e5
//double const K1 = (pbesk - p0) / dens;
//double const K2 = 2 * sigma / dens;
//double const K3 = 4 * mu / dens;
long double p0 = pbesk + 2;
long double dt = 1e-10/tm, t = 0;
long double const pi = 3.14;
long double f = tm*21000;
long double T = 1/f;
long double dP();
double force(double x,double a) {
	
	return  (a * x);
}
//void eler() {
//	for (int i = 0; i < n; i++) {
//		F = -force(x, a);
//		v = v_ + dt * F / m;
//		x = v * dt + x_;
//		t += dt;
//		file << t << "\t" << x << endl;
//	}
//}
long double f_usk() {
	return ((1 + v / vzv) * (pb + pa - pbesk) - 1.5 * v * v * (1 - v / (vzv * 3)) + dP()*x/vzv) / (x * (1 - v / vzv) + 4*mu /vzv);
}
void davl() {	
	//pg = p0 * pow((1 / x), k * 3);
	pb = p0 * pow(x, -k * 3) - 2 /x - 4 * mu * v/x;
}
long double dP() {
	return -3* p0 * k* pow(x, -3 * k - 1) * v + 2*v / (x*x) + 4 * mu * v*v / (x*x);
}
void iter(long double dt) {
	for (int i = 0; i < 30; i++) {
		//F = -force(x, a);
		//double F_ = -force(x_, a);
		//v = v_ + dt / m * (F + F_) / 2;
		//x = (v_ + v) * dt / 2 + x_;
		//if (v * v_ <= 0) {
		//	F = -force(x, a);
		//	F_ = -force(x_, a);
		//	/*file << t << "\t" << x_ - 1 / 2 * m * v_ * v_ / (F) << endl;*/
		//	if(i == 0)
		//		file << t << "\t" << x_ - m * v_ * v_ / (F + F_) << endl;
		//}
		
		davl();
		pa = pA* cos(2 * pi * f * t);
		
		//usk = ((1 + v/vzv) * (pb + pa - pbesk) - 1.5 * v * v * (1 - v / (vzv *3))) / (x * (1 - v / vzv));
		//usk = ((1 + v / vzv) * (pb + pa - pbesk) - 1.5 * v * v * (1 - v / (vzv * 3)) + x/vzv * (pb - pb_)/dt) / (x * (1 - v / vzv));

		usk = f_usk();

		//usk = ((1 + v / vzv)* (pb + pa - pbesk) / dens - 1.5 * v * v * (1 - v / (3 * vzv))) / ((1 - v / vzv) * x);// Gaitan		
		//usk = (1 / dens * (pb - pa - pbesk)  - 1.5* v * v  ) / (x);// no visc no tension
		//double f1 = 1.5 / x;
		//double f2 = K3 / (x*x);
		//double f3 = K1 / x + K2 / (x*x);
		//usk = -(f2 * v + v*v*f1 + f3); // all
		//usk = -(1.5 / x * v*v + K1 / x + K2 / (x*x)); // no visk

		v = v_ + 0.5 * (usk + usk_)* dt;
		if (v != v)
			exit(2);

		x = x_ + 0.5 * (v + v_) * dt;
		if (x != x)
			exit(3);

	}
}
void nach() {
	x = 1;
	x_ = x;
	davl();
	pb_ = pb;
	
}
void kronk() {
	int i = 0;
	nach();
	while(t < T) {

		if (i % 10 == 0)
			file << t*f <<"\t"<< x << endl;

		iter(dt);
		/*if (v * v_ <= 0) {
			double x_e = x;
			double usk_e = usk;
			double dt_e = dt;
			for (int i = 0; i < 40; i++) {
				dt_e = -v_ / (usk_ + usk_e);
				x_e = x_ - v_ * v_ / (usk_ + usk_e);
				pg = p0 * pow((x0 / x_e), k * 3);
				pb = pg - 2 * sigma / x_e;
				pa = pA * cos(2 * pi * f *t);
				usk_e = 1 / dens * (pb - pa - pbesk) / x_e + x_e / dens / vzv * (pb-pb_)/dt_e;
			}
			file << (t + dt_e)<< "\t" << x_e/x0 << endl;
		}*/

		/*if (x < 0 || x > x_) {
			file << t <<"\t" << x_;
			cout << x << endl;
			cout << t;
			break;
		}*/
		if (x != x || v != v || x < 0)
			exit(1);

		x_ = x;
		v_ = v;
		t += dt;
		pb_ = pb;
		i++;
		usk_ = usk;		
	}
}

int main() {

	kronk();	
	cout  << "Completed\n";
	file.close();
	system("python gai.py");	
}